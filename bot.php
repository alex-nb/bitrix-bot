<?php

$appsConfig = array();
$configFileName = '/config_' . trim(str_replace('.', '_', $_REQUEST['auth']['domain'])) . '.php';
if (file_exists(__DIR__ . $configFileName)) {
   include_once __DIR__ . $configFileName;
}

switch ($_REQUEST['event']) {
	
	case 'ONIMBOTMESSAGEADD':
		// check the event - register this application or not
		if (!isset($appsConfig[$_REQUEST['auth']['application_token']])) {
		  return false;
		  break;
		}
		// send answer message
		$result = restCommand('imbot.message.add', 
		  array(
			 "DIALOG_ID" => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
			 "MESSAGE"   => "Как круто, что вы написали. Посмотрите список доступных комманд: [send=/help]открыть[/send] \n",
		  ), 
		  $_REQUEST["auth"]);
		  break;
		  
		case 'ONAPPINSTALL':
		// handler for events
		$handlerBackUrl = 'https://bitrixapps.pinkomp.ru/test-bot/bot.php';
		// register new bot
		$result = restCommand('imbot.register', array(
		   'CODE' => 'TestingBot',
		   'TYPE' => 'B', 
		   'EVENT_MESSAGE_ADD'     => $handlerBackUrl,
		   'EVENT_WELCOME_MESSAGE' => $handlerBackUrl,
		   'EVENT_BOT_DELETE'      => $handlerBackUrl,
		   'PROPERTIES' => array(
			  'NAME'  => 'Тестировщик',
			  'COLOR'  => 'MINT',
			  'PERSONAL_BIRTHDAY' => '2019-06-28',
			  'WORK_POSITION'  => 'Провожу тесты',
			  'PERSONAL_PHOTO'  => 'iVBORw0KGgoAAAANSUhEUgAAAcQAAAHECAMAAACQgaotAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMwAAZgAAmQAAzAAA/wAzAAAzMwAzZgAzmQAzzAAz/wBmAABmMwBmZgBmmQBmzABm/wCZAACZMwCZZgCZmQCZzACZ/wDMAADMMwDMZgDMmQDMzADM/wD/AAD/MwD/ZgD/mQD/zAD//zMAADMAMzMAZjMAmTMAzDMA/zMzADMzMzMzZjMzmTMzzDMz/zNmADNmMzNmZjNmmTNmzDNm/zOZADOZMzOZZjOZmTOZzDOZ/zPMADPMMzPMZjPMmTPMzDPM/zP/ADP/MzP/ZjP/mTP/zDP//2YAAGYAM2YAZmYAmWYAzGYA/2YzAGYzM2YzZmYzmWYzzGYz/2ZmAGZmM2ZmZmZmmWZmzGZm/2aZAGaZM2aZZmaZmWaZzGaZ/2bMAGbMM2bMZmbMmWbMzGbM/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kAZpkAmZkAzJkA/5kzAJkzM5kzZpkzmZkzzJkz/5lmAJlmM5lmZplmmZlmzJlm/5mZAJmZM5mZZpmZmZmZzJmZ/5nMAJnMM5nMZpnMmZnMzJnM/5n/AJn/M5n/Zpn/mZn/zJn//8wAAMwAM8wAZswAmcwAzMwA/8wzAMwzM8wzZswzmcwzzMwz/8xmAMxmM8xmZsxmmcxmzMxm/8yZAMyZM8yZZsyZmcyZzMyZ/8zMAMzMM8zMZszMmczMzMzM/8z/AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8AZv8Amf8AzP8A//8zAP8zM/8zZv8zmf8zzP8z//9mAP9mM/9mZv9mmf9mzP9m//+ZAP+ZM/+ZZv+Zmf+ZzP+Z///MAP/MM//MZv/Mmf/MzP/M////AP//M///Zv//mf//zP///69WTRwAAAAodFJOUzaz42qTU8uFRv7Co/N601u8TOurmjyJ2nJiAAAAAAAAAAAAAAAAAAD5G8MvAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAQ60lEQVR4Xu3d6UIiSRBF4XHXFndF5f0fdFiuyhKZGRlVURrB/f7MdHcWkBwRKJKq/xYUHiMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICeSNeP3ycv96+nDw+nrzcvp5/PFzjH/LJGPFz/nh6Izo9mX9iUCbJIr7dncwQrGh2cveG4Ulkinhx2wz4ZXZ7gY0ySBPx4kVdcGP2kqZjkohXz0jT5fQKmweXIuIJohi84CJCSxBxQMKVE1xMYOEjDky4Ej5j8IhX6DBQ8OfG0BGfCu/p+50+4SJDihxxjgKjmONCIwoccYRnw22BnxnjRnzBnT+aW1xwPGEjnuGuH9EZLjqcsBE7d7JpzHDR4USNOPIT4kbUp8WgEd9wt48s6EdUQSM+4l4f2SMuPpigEXGnjw4XH0zMiK+4z0f3iiuIJWZEh5emGzFfoIaM+IG73MEHriKUkBHvcY87eMdVhBIx4jXucBcRV6dGjOjyRv9LxDf8ESPi7naCK4kkYMRb3NtOAn6Y8YsR3y4+z+4McGe3zd4fb1/nS6+3j+/6dyW4mi5nnxe/uMvudyJenP8zLRRVm53cHSy4eFIs8R/k+fL1Adc1rV+IOHd8h7B2f1Wc1X9X7xjk5X4+/V069TV+eBecneOais59H4/Ljne4pqlMG9H9/ntW7XH58P1dvtT8SRrVlBHdE87UD4Er75sy6Yvc6SKeud9v/3BNKv+wkRv9T9Rgk0W8xNz8dN5p+rcqVvdTveuYKKLj5w4w617Dfe3+q6H358pqmoiuezvXZoaf+jf/itOs95gkovd7syXTt34fsLGjU1yVqwki/uf+gt78WnD0VeSHZhN8tuUfcYKnHvtHD9jclf+hAdwjTtLwGVfWbYob51/RPeIkd9Mlrqyb/xufJcuLri7eESd4Plzqepu/zf0t/5r3GjrniBO8Ll354xG9X6P6RnT+EP7bX4/ovHLHNeInpuDuz0f0Xc/qGnGSFzUrfz+i6/orz4j+O9u+BIhovokKjhEvcPMnECDijePyG8eIox1kpi1CRPMOiTa/iP6fPv2IENHxcym/iBM+EGNE9HsoukV0OERJWYiIfm8z3CJO+UAMEtFtv41XRNevnx2IEfHG6yiAXhF1H7c+3z/uqO0dOMUYiXmZ5zkuQFL7XTLDGLjX7ef3OsyxV8T2zprT+eFn3rUHxuTHJK0thzt86F+fK55AMHZsThGbq1dOxQOpBY64dNbM6HRmFaeIrY8vCr9YYkdsP4k4LQt3itj4mSy9740esbUi2en1qVNE3OiCYo/wEVtHQ8aokflErL/TL39CGj9i46MbnydFn4jnuM2iyoqTBBHrr8t9vvLmE7H641ipkSFi9SwPPss0fCJW10dhjCRDxOrrAZ8DVvlExE0W1b5jkiJi9VisGDOu6SPW9uWniFh9VYcx45o+Ym05dIqI1UNbY8y4po+IIaIUEc2zN3OJaP5ZzB/R5WsZjFjEiGUYI2JEE0YsYsQyjBExogkjFjFiGcaIGNGEEYsYsQxjRIxowohFjFiGMSJGNGHEIkYswxgRI5owYhEjlmGMiBFN4kT8UxixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0YQRTRixjBFNGNGEEcsY0eRvRfzvOojq3YaJio4gYg6YqIgRg8BERYwYBCYqYsQgMFERIwaBiYoYMQhMVMSIQWCiIkYMAhMVMWIQmKiIEYPAREWMGAQmKmLEIDBRESMGgYmKGDEITFTEiEFgoiJGDAITFTFiEJioiBGDwERFjBgEJipixCAwUREjBoGJihgxCExUxIhBYKIiRgwCExUxYhCYqIgRg8BERYwYBCYqYsSR3OG/Wr13PCYqYsRxnNyc4P90Tmed9zwmKmLEUdwub0P1m2l7nm9uOitu5iljxDG8rm/EJf7Utmy4rNh1N62voYARR3COW6GtuG64rIg/qmw2kTHicFe4ETc39/ibOjS8uXnGX2hgExEjDvaB27Byir+reJth7FJHRWwhYsShznATNpoVtxv2VMQGIkYc6AG34MtzffK7DVUP3Q2MFzHiMBe4AT+qbx2u9xrqK2K4iBEHucb1b5s94R8PHTZUV8RoESMOsf/LER7wz/ue8O+73vGvdRgsYsQh5IY3N5/4912Hv3o3VBUxVsSIA3y/4TvwgRHbSg11FTFUxIh2p7hyyeGHGvsvY7cpdhJgpIgRzWoND8/g/4m/l7UrYqCIEa3ucdUlrxi3sbtL4FCzIsaJGNHoEtdcdouRK62G7Z3nGCZiRBvN8Td/Pibe3r1a0vg4EqNEjGjyiOute8ToO/y5rl4Rg0SMaPGCq23ZdNE13H8W3YMxIka0eCq9y9+3eqb7+bixrv5uEYNEjGgi7QQVvS/m+L+WxutTjBIxok1hr+mhnsdsDYaJGNGqvM/Nork8B+NEjGhW32PTp73cEQNFjGj3jisfTrFkFSNFjDjAWBW/3k/WYKjoCCJaz4uhmEV735uGpuGxR7SeoUbxO26Us9/ovsSBwSJGLNJEVO5/q1F+EQejRYxYpIq4OMFoqxdcTguGixixSBdx/YUou+3Pq6owXsSIRcqI+EqUjbohI9poI35/Kapf9XOLXdhCxIhF6ojqzyn2nWN7DWwiYsQifUT1J4a7ehoyok1HRMUamkNzbKuDjUSMWNQTsbEiUdLXkBFtuiJW1nfL9teltmAzESMW9UXUL9lY6z3sDSPadEbUL9lYkr6rUYcNRYxY1BtRv2TD0JARbbojqpdsnGF4D2wqYsQiQ0Tdkg1LQ0a0sUTUfNgvfwO1BRuLGLHIFLH5Zanid8EbsLWIEYtsEVtLNowNGdHGGLF+fRcY1A3bixixyBqxtmSjfHSUFlyAiBGLzBGLSzZm1xhggIsQMWKRPWJhycaQhoxoMyCiuGSj94DRu3AhIkYsGhJRWLIxrCEj2gyKeLBko+9g0YdwMSJGLBoWcW/JRs9BhkW4HBEjFg2MuLNkY3DDY4/4a36WbAxvyIi/5WvJxggNGfHXbJZsqA8RXbOeZgEjulp92D9KQ0b8RW+zcRoyYgaYqIgRg8BERYwYBCYqYsQgMFERIwaBiYoYMQhMVMSIQWCiIkYMAhMVMWIQmKiIEYPAREWMGAQmKmLEIDBRESMGgYmKGDEITFTEiEFgoiJGDAITFTFiEJioiBGDwERFjBgEJipixCAwUREjBoGJihgxCExUxIhBYKIiRgwCExUxYhCYqIgRR9RxDNO33uNKYaIiRhzPpfpMJavvaHRWxERFjDia1aGklBXXB9Xsq7ieZgEjjmVzODBVRRwYtavieosCRhzJ1yHdFBW/D27bUxGbiBhxHD+H5WtW/G7YVRFbiBhxFNuHVmycS2+rYU9FbCBixDFsN2xU3GnYURHjRYw4gt2G1Yp7DfUVMVzEiMPtN6xUPGiorojRIkYc7LBhsaLQUFsRg0WMOJTUsFCxcPoTVUWMFTHiQHJDsWLxFDaaihgqYsRhSg2Fs+dXTkOkqIiRIkYcpNzwoGL1VFLtihgoYsQhag33KjZO6tasiHEiRhyg3nCnYvOUbq2KGCZiRLtWw62KzYbNihglYkSzdsPvitf4Y1W9IgaJGNFK0xAVn/CHhmpFjBExopXqLHvrisqG9VO2Y4yIEc2UZ7z8pz1ldP20+xgkYkQ7ZUWlekNG9DJmxUZDRnQzXsVXXGIRxokYcZCxKjYbHnvEhzsb1TlHx6nYbnjsEX3PUDNGRUVDRrRRnmZoeEVNQ0a00Z4ramjFW1xOHQaLGLFIfcKvYRV1DRnRRn/WtiEVlQ0Z0UYfcUBFbUNGtOmIuGh/XChTN2REm56Ixor6hoxo0xXRVLGjISPa9EX8r79iT0NGtOmL2F+xqyEj2nRG7K2oPkjDBrYSMWJRb8S+ip0NGdGmO2JPxd6GjGjTH1H+6pqkuyEj2hgiate1ba8NV8KGIkYs6o+obmioiO1EjFjUHbGjYX9FbCZixKLeiKq1+j86K2IrESMWdUZUfGdmV19FbCRixKK+iN0NOytiGxEjFnVFbHyHVNZTEZuIGLGoJ6LhcbjSURFbiBixqCOisWFPRWwgYsQifURzw+oRxHZhvIgRi9QRBzTUV8RwESMWaSOqd5jKlBUxWnQEEc8fbVpfNoOBDbUVMVh0BBF9DW6orIixIkYcZoSGuooYKmLEQUZpqKqIkSJGHGKkhpoPiTFQxIgDjNZQURHjRIxoN2LDdkUMEzGi2agNmxUxSsSIVtqlbS/a2PWKGCRiRCt9Gm3u6pJwjBExotUjrrpu/fBSPhaf1pdbgDEiRjTTVMSDS/VYrDZkRCftit9Pc4qK9YaM6KVVceulSrNioyEjuqlX3Hmh0qjYasiIfmoV919s1io2GzKio3LFwzcM5WNstBsyoqdSRelNX6mioiEjupIrSg1LFTUNGdGXVFFuKFdUNWREZ4cVSw2lirqGjOhtv2Lt2Jf7FZUNGdHdbsX68Ut3K2obMqK/7YqtY9BuV1Q3ZMQJ/FRsH0f4p6K+ISNO4aui5ljQXxU7GjLiJDYVNQ2/KvY0ZMRprCoql/6vK3Y1ZMSJPKobrs751teQEadyhv9qdDZkxAwwUREjBoGJihgxCExUxIhBYKIiRgwCExUxYhCYqIgRg8BERYwYBCYqYsQgMFERIwaBiYoYMQhMVMSIQWCiIkYMAhMVMWIQmKgoTsTqNDAkMUxUhCHjmj6iy8/iXzL976HpI95hTFofmKgIY8Y1fcSe43WHVD1oK8aMyyfiO26yCGPSwjRF7xgzLp+IJ7jNIv0SpZDOMU2R8tjFnXwiXuE2yzAoKUxSNsegcflEfMBtlvWdtCeY7a96HPrEqHH5RKz/OCrXXodU/WXq9UvIKWL1lY3Xb5U/YI4JFpxi2MicIr7iVpck/Y1afUG35PQryCniBW510Szhe/6P7a8zii4wcmROERVHp3x+7V0e/6ddnzcT3swwdmxeEVu/Tzee73F2kuDu2wFXvF7QeUWs7gU+Vl77/r0itl6fHiOffW5LbhE/ccvpm887/SW3iHwo7nN6k7jkF/EMt52g53utffwi8qG4y+0Z0TXiNW49rTm+K3aMuLjFzael+vlQhvGMqD2pyDHw2lmz5hqxuQf1eDjtNd1wjdj6dO14+C5J8Y24uMQkjtwl7g4nzhEXp5jGUfN7m7/hHXHnkKFH6hl3hRv3iNqT2eU187+P8V8/x15x5v/lE/+IR/686P18uDJFxMU9JnSEHPeY/pgk4vHugPPc2fZjmoiLz6N8Ypz5ffq0Y6KIjS985TTZl/gmi7h4OLLXN89uqzEOTBdxsZgf0e/U2ZTfVJgy4mJxdST7b2ZXmPA0po24fIVzBM+N/yZ6PfNt6ohLd6k7Xk77IFz7hYhLT/PHhC9znv+du372W/Q7ETeeHs7uruYJXN2dPVz84j35mxFpJIyYACMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICjJgAIybAiAkwYgKMmAAjJsCICTBiAoyYACMmwIgJMGICjJgAIybAiOEtFv8DRuyb2IdRXYIAAAAASUVORK5CYII='
		   ),
		), $_REQUEST["auth"]);
		// save params
		$appsConfig[$_REQUEST['auth']['application_token']] = array(
		   'BOT_ID'      => $result['result'],
		   'LANGUAGE_ID' => $_REQUEST['data']['LANGUAGE_ID'],
		);
		saveParams($appsConfig);
		break;
	
	case 'ONIMCOMMANDADD':
		// check the event - register this application or not
	   if (!isset($appsConfig[$_REQUEST['auth']['application_token']])) {
		  return false;
		  break;
	   }
	   foreach ($_REQUEST['data']['COMMAND'] as $id_command=>$data_command) {
		   $main_keyboard = [
			  	["TEXT" => "Проверить тесты для прохождения", "COMMAND" => "check-test", "DISPLAY" => "LINE"],
				["TEXT" => "Список всех команд", "COMMAND" => "help", "DISPLAY" => "LINE"],
		   ];
		   switch ($data_command['COMMAND']) {
				case 'help':
					$result = restCommand('imbot.command.answer', Array(
						'COMMAND' => $data_command['COMMAND'],
						'MESSAGE_ID' => $data_command['MESSAGE_ID'], 
						'MESSAGE' => 'Доступные команды представлены ниже!' ,
						'KEYBOARD' => $main_keyboard
					), $_REQUEST["auth"]);
					break;
					
				case 'check-test':
					$result = restCommand('imbot.command.answer', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => 'Сейчас для прохождения есть следующие тесты',
							'KEYBOARD' => Array(
									  Array("TEXT" => "Тест 1",
											"BG_COLOR" => "#29619b",
											"TEXT_COLOR" => "#fff",
											"COMMAND" => "start-test",
											"COMMAND_PARAMS" => json_encode(["type" => 0, "title" => "Тест 1", "id" => "123"]),
											"DISPLAY" => "LINE"),
									  Array("TYPE" => "NEWLINE"),
									  Array("TEXT" => "Тест 2",
											"BG_COLOR" => "#29619b",
											"TEXT_COLOR" => "#fff",
											"COMMAND" => "start-test",
											"COMMAND_PARAMS" => json_encode(["type" => 0, "title" => "Тест 2", "id" => "456"]),
											"DISPLAY" => "LINE"),
							   )
					), $_REQUEST["auth"]);
					break;

				case 'start-test':
					//file_put_contents("test.txt", "data: ".$data_command['COMMAND_PARAMS']."\n");
					$data_test = json_decode($data_command['COMMAND_PARAMS']);
				   switch($data_test->type) {
					   case 0:
						   $result = restCommand('imbot.command.answer', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => 'Вы выбрали [B]'.$data_test->title.'[/B]',
						   	'KEYBOARD' => Array(
								Array('TEXT' => 'Начать',
								  	"BG_COLOR" => "#29619b",
									"TEXT_COLOR" => "#fff",
									"COMMAND" => "start-test",
									"COMMAND_PARAMS" => json_encode(["type" => 1, "id" => "123"]),
									"DISPLAY" => "LINE")
							)
					), $_REQUEST["auth"]);
						   break;
					   case 1:
						   $results = [];
						   $keyboard = [
						   	   ['TEXT' => '1', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 2, "id" => "123", "idQ" => 1, "correct" => false, 'results' => $results])],
							   ['TEXT' => '3', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 2, "id" => "123", "idQ" => 1, "correct" => false, 'results' => $results])],
							   ['TEXT' => '4', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 2, "id" => "123", "idQ" => 1, "correct" => true, 'results' => $results])],
							   ['TEXT' => 'Буква И', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 2, "id" => "123", "idQ" => 1, "correct" => false, 'results' => $results])]
						   ];
						   $result = restCommand('imbot.command.answer', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => '[B] Сколько будет 2+2? [/B]',
						   	'KEYBOARD' => $keyboard
					), $_REQUEST["auth"]);
						   break;
					   case 2:
						   $results = (array) $data_test->results;
						   $results[$data_test->idQ] = $data_test->correct;
						   $keyboard = [
						   	   ['TEXT' => 'Кошка', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 3, "id" => "123", "idQ" => 2, "correct" => false, 'results' => $results])],
							   ['TEXT' => 'Голубь', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 3, "id" => "123", "idQ" => 2, "correct" => true, 'results' => $results])],
							   ['TEXT' => 'Рыба', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 3, "id" => "123", "idQ" => 2, "correct" => false, 'results' => $results])],
							   ['TEXT' => 'Диван', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 3, "id" => "123", "idQ" => 2, "correct" => false, 'results' => $results])]
						   ];
						   $result = restCommand('imbot.message.update', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => '[B] Что из перечисленного может летать (с помощью крыльев)? [/B]',
						   	'KEYBOARD' => $keyboard
					), $_REQUEST["auth"]);
						   break;
					   case 3:
						   $results = (array) $data_test->results;
						   $results[$data_test->idQ] = $data_test->correct;
						   $keyboard = [
						   	   ['TEXT' => '3 слона', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 4, "id" => "123", "idQ" => 3, "correct" => false, 'results' => $results])],
							   ['TEXT' => '100 грамм', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 4, "id" => "123", "idQ" => 3, "correct" => false, 'results' => $results])],
							   ['TEXT' => '1 тонна', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 4, "id" => "123", "idQ" => 3, "correct" => false, 'results' => $results])],
							   ['TEXT' => '1 кг', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 4, "id" => "123", "idQ" => 3, "correct" => true, 'results' => $results])]
						   ];
						   $result = restCommand('imbot.message.update', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => '[B] Сколько весит 1кг гвоздей? [/B]',
						   	'KEYBOARD' => $keyboard
					), $_REQUEST["auth"]);
						   break;
					   case 4:
						   $results = (array) $data_test->results;
						   $results[$data_test->idQ] = $data_test->correct;
						   $keyboard = [
						   	   ['TEXT' => '2', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 5, "id" => "123", "idQ" => 4, "correct" => true, 'results' => $results])],
							   ['TEXT' => '13', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 5, "id" => "123", "idQ" => 4, "correct" => false, 'results' => $results])],
							   ['TEXT' => '4', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 5, "id" => "123", "idQ" => 4, "correct" => false, 'results' => $results])],
							   ['TEXT' => '0', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 5, "id" => "123", "idQ" => 4, "correct" => false, 'results' => $results])]
						   ];
						   $result = restCommand('imbot.message.update', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => '[B] Сколько в норме глаз у человека [/B]',
						   	'KEYBOARD' => $keyboard
					), $_REQUEST["auth"]);
						   break;
					   case 5:
						   $results = (array) $data_test->results;
						   $results[$data_test->idQ] = $data_test->correct;
						   $keyboard = [
						   	   ['TEXT' => 'Нормально', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 6, "id" => "123", "idQ" => 5, "correct" => true, 'results' => $results])],
							   ['TEXT' => 'Хорошо', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 6, "id" => "123", "idQ" => 5, "correct" => true, 'results' => $results])],
							   ['TEXT' => 'Плохо', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 6, "id" => "123", "idQ" => 5, "correct" => true, 'results' => $results])],
							   ['TEXT' => 'Не знаю', 'COMMAND'=> 'start-test', 'COMMAND_PARAMS' => json_encode(["type" => 6, "id" => "123", "idQ" => 5, "correct" => true, 'results' => $results])]
						   ];
						   $result = restCommand('imbot.message.update', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => '[B] Как дела? [/B]',
						   	'KEYBOARD' => $keyboard
					), $_REQUEST["auth"]);
						   break;
					   case 6:
						   $results = (array) $data_test->results;
						   $results[$data_test->idQ] = $data_test->correct;
						   $col = 0;
						   $all = 0;
						   foreach ($results as $answer) {
							   if ($answer) {
								   $col++;
							   }
							   $all++;
						   }
						   $result = restCommand('imbot.message.update', Array(
							'COMMAND' => $data_command['COMMAND'],
							'MESSAGE_ID' => $data_command['MESSAGE_ID'],
							'MESSAGE' => 'Тест завершен, спасибо. Ваш результат: [B]'.$col.'/'.$all.'[/B]',
							'KEYBOARD' => $main_keyboard
					), $_REQUEST["auth"]);
						   break;
				   }
					break;

		   }
	   }
		break;
	
		  
	  case 'ONIMBOTDELETE':
		// check the event - register this application or not
         if (!isset($appsConfig[$_REQUEST['auth']['application_token']])) {
            return false;
         }
         // unset application variables
         unset($appsConfig[$_REQUEST['auth']['application_token']]);
         // save params
         saveParams($appsConfig);
		 break;
}
 
 /**
 * Save application configuration.
 *
 * @param $params
 *
 * @return bool
 */
function saveParams($params) {
   $config = "<?php\n";
   $config .= "\$appsConfig = " . var_export($params, true) . ";\n";
   $config .= "?>";
   $configFileName = '/config_' . trim(str_replace('.', '_', $_REQUEST['auth']['domain'])) . '.php';
   file_put_contents(__DIR__ . $configFileName, $config);
   return true;
}


/**
 * Send rest query to Bitrix24.
 *
 * @param       $method - Rest method, ex: methods
 * @param array $params - Method params, ex: array()
 * @param array $auth   - Authorize data, ex: array('domain' => 'https://test.bitrix24.com', 'access_token' => '7inpwszbuu8vnwr5jmabqa467rqur7u6')
 *
 * @return mixed
 */
function restCommand($method, array $params = array(), array $auth = array()) {
   $queryUrl  = 'https://' . $auth['domain'] . '/rest/' . $method;
   $queryData = http_build_query(array_merge($params, array('auth' => $auth['access_token'])));
   // writeToLog(array('URL' => $queryUrl, 'PARAMS' => array_merge($params, array("auth" => $auth["access_token"]))), 'ReportBot send data');
   $curl = curl_init();
   curl_setopt_array($curl, array(
      CURLOPT_POST           => 1,
      CURLOPT_HEADER         => 0,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL            => $queryUrl,
      CURLOPT_POSTFIELDS     => $queryData,
   ));
   $result = curl_exec($curl);
   curl_close($curl);
   $result = json_decode($result, 1);
	if (array_key_exists('error', $result)) {
		file_put_contents("test.txt", $result['error_description']."\n", FILE_APPEND);
	}
   return $result;
}
/**
 * Write data to log file.
 *
 * @param mixed  $data
 * @param string $title
 *
 * @return bool
 */
function writeToLog($data, $title = '') {
   $log = "\n------------------------\n";
   $log .= date("Y.m.d G:i:s") . "\n";
   $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
   $log .= print_r($data, 1);
   $log .= "\n------------------------\n";
   file_put_contents(__DIR__ . '/imbot.log', $log, FILE_APPEND);
   return true;
}


/*
$result = restCommand('imbot.command.register', Array(
   'BOT_ID' => 576, // Идентификатор чат-бота владельца команды
   'COMMAND' => 'help', // Текст команды, которую пользователь будет вводить в чатах
   'COMMON' => 'N', // Если указан Y, то команда доступна во всех чатах, если N - то доступна только в тех, где присутствует чат-бот
    'HIDDEN' => 'N', // Скрытая команда или нет - по умолчанию N
   'EXTRANET_SUPPORT' => 'N', // Доступна ли команда пользователям Экстранет, по умолчанию N
   'LANG' => Array( // Массив переводов, желательно указывать, как минимум, для RU и EN
      Array('LANGUAGE_ID' => 'en', 'TITLE' => 'Get list commands'),
          Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Получить список команд'),
   ),
   'EVENT_COMMAND_ADD' => 'https://bitrixapps.pinkomp.ru/test-bot/bot.php', // Ссылка на обработчик для команд

), $_REQUEST["auth"]);

$result = restCommand('imbot.command.register', Array(
   'BOT_ID' => 576, // Идентификатор чат-бота владельца команды
   'COMMAND' => 'check-test', // Текст команды, которую пользователь будет вводить в чатах
   'COMMON' => 'N', // Если указан Y, то команда доступна во всех чатах, если N - то доступна только в тех, где присутствует чат-бот
    'HIDDEN' => 'N', // Скрытая команда или нет - по умолчанию N
   'EXTRANET_SUPPORT' => 'N', // Доступна ли команда пользователям Экстранет, по умолчанию N
   'LANG' => Array( // Массив переводов, желательно указывать, как минимум, для RU и EN
      Array('LANGUAGE_ID' => 'en', 'TITLE' => 'Get list tests'),
          Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Получить список тестов для прохождения'),
   ),
   'EVENT_COMMAND_ADD' => 'https://bitrixapps.pinkomp.ru/test-bot/bot.php', // Ссылка на обработчик для команд

), $_REQUEST["auth"]);

$result = restCommand('imbot.command.register', Array(
   'BOT_ID' => 576, // Идентификатор чат-бота владельца команды
   'COMMAND' => 'start-test', // Текст команды, которую пользователь будет вводить в чатах
   'COMMON' => 'N', // Если указан Y, то команда доступна во всех чатах, если N - то доступна только в тех, где присутствует чат-бот
    'HIDDEN' => 'Y', // Скрытая команда или нет - по умолчанию N
   'EXTRANET_SUPPORT' => 'N', // Доступна ли команда пользователям Экстранет, по умолчанию N
   'LANG' => Array( // Массив переводов, желательно указывать, как минимум, для RU и EN
      Array('LANGUAGE_ID' => 'en', 'TITLE' => 'Start testing'),
          Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Начать тестирование'),
   ),
   'EVENT_COMMAND_ADD' => 'https://bitrixapps.pinkomp.ru/test-bot/bot.php', // Ссылка на обработчик для команд

), $_REQUEST["auth"]);
*/
